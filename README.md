# Course: BPC2T - seminar 7
This project provides a solution for seminar 7. Includes topics like:
1. Class creation 
2. Advanced Data Types (Collection classes - Map, List, Set)
3. Methods from Object class (toString, equals, hashCode)
4. Writing to a text file
5. Sorting objects Comparable, Comparator
6. Relationships between objects.

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```