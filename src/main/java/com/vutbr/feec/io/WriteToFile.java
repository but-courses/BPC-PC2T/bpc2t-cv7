package com.vutbr.feec.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.vutbr.feec.model.Student;

/**
 * @author Pavel Seda
 */
public class WriteToFile {

    public void writeToFile(File file, List<Student> students) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("#id,name,age,year");
            for (Student student : students) {
                bw.write(System.lineSeparator());
                bw.write(student.getId() + "," + "," + student.getEmail() + "," + student.getName() + "," + student.getBirth());
            }
        } catch (IOException e) {
            // handle exception ... (log or rethrow)
            throw new RuntimeException("Cannot write to file.." + e);
        }
    }
}
