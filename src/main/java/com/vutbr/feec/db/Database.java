package com.vutbr.feec.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.vutbr.feec.model.Student;

/**
 * Databáze obsahující studenty.
 *
 * @author Pavel Seda
 */
public class Database {

    private List<Student> students = new ArrayList<>();

    public Database() {
    }

    public Database(List<Student> students) {
        super();
        setStudents(students);
    }

    /**
     * Výpiše všechny studenty v databázi do konzole.
     */
    public void printAllStudents() {
        for (Student student : students) {
            System.out.println(student);
        }
    }

    /**
     * Odstraní studenta z databáze. Nutno implementovat přes iterátor, jinak vyhodí
     * java.util.ConcurrentModificationException
     *
     * @param id studenta, kterého chci odstranit z databáze
     */
    public void removeStudentById(long id) {
        for (Iterator<Student> iter = students.listIterator(); iter.hasNext(); ) {
            Student student = iter.next();
            if (student.getId() == id) {
                iter.remove();
            }
        }
    }

    public List<Student> getStudents() {
        return students;
    }

    public boolean addStudent(Student student) {
        return this.students.add(student);
    }

    public void setStudents(List<Student> students) {
        this.students = new ArrayList<>(students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(students);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Database)) {
            return false;
        }
        Database other = (Database) obj;
        return Objects.equals(students, other.getStudents());
    }

    @Override
    public String toString() {
        return "Database [students=" + students + "]";
    }

}
