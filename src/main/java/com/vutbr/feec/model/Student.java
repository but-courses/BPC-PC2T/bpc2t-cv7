package com.vutbr.feec.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Třída reprezentující informace, které evidujeme o studentovi.
 *
 * @author Pavel Seda
 */
public class Student implements Comparable<Student> {

    private Long id;
    private String email;
    private String name;
    private LocalDate birth;

    public Student(Long id, String email, String name, LocalDate birth) {
        super();
        setId(id);
        setEmail(email);
        setName(name);
        setBirth(birth);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    /**
     * Comparable based on name..
     */
    @Override
    public int compareTo(Student student) {
        return this.getName().compareTo(student.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(getEmail(), student.getEmail());
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", birth=" + birth +
                '}';
    }
}
