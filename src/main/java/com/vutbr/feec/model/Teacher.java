package com.vutbr.feec.model;

import java.time.LocalDate;
import java.util.*;

/**
 * @author Pavel Seda
 */
public class Teacher {

    private Long id;
    private String email;
    private String name;
    private LocalDate birth;
    private Map<String, Set<Student>> studentsPerCourse = new HashMap<>();

    public Teacher(Long id, String email, String name, LocalDate birth) {
        setId(id);
        setEmail(email);
        setName(name);
        setBirth(birth);
    }

    public Teacher(Long id, String email, String name, LocalDate birth, Map<String, Set<Student>> studentsPerCourse) {
        setId(id);
        setEmail(email);
        setName(name);
        setBirth(birth);
    }

    public Teacher(long id, String name, int age, Map<String, Set<Student>> studentsPerCourse) {
        setId(id);
        setEmail(email);
        setName(name);
        setBirth(birth);
        setStudentsPerCourse(studentsPerCourse);
    }

    public Map<String, Set<Student>> getStudentsPerCourse() {
        return studentsPerCourse;
    }

    public void setStudentsPerCourse(Map<String, Set<Student>> studentsPerCourse) {
        if (studentsPerCourse == null) {
            throw new NullPointerException("Given map could not be null.. in method: setStudentsPerCourse()");
        }
        // replace null values to TreeSet values if necessary
        for (Map.Entry<String, Set<Student>> entry : studentsPerCourse.entrySet()) {
            String key = entry.getKey();
            Set<Student> students = entry.getValue();
            if (students == null) {
                studentsPerCourse.put(key, new TreeSet<Student>());
            }
        }
        this.studentsPerCourse = studentsPerCourse;
    }

    public void addCourse(String course) {
        if (studentsPerCourse.containsKey(course)) {
            throw new IllegalArgumentException("This course already exists.");
        }
        this.studentsPerCourse.put(course, new TreeSet<Student>());
    }

    public void addCourseAndStudents(String course, Set<Student> students) {
        if (studentsPerCourse.containsKey(course)) {
            throw new IllegalArgumentException("This course already exists.");
        }
        this.studentsPerCourse.put(course, students);
    }

    public void addCourseAndStudent(String course, Student student) {
        if (studentsPerCourse.containsKey(course)) {
            throw new IllegalArgumentException("This course already exists.");
        }
        Set<Student> studentSet = new TreeSet<>();
        studentSet.add(student);
        this.studentsPerCourse.put(course, studentSet);
    }

    public void assignStudentToCourse(Student student, String course) {
        if (!studentsPerCourse.containsKey(course)) {
            throw new IllegalArgumentException("Cannot assign student to course which do not exists.");
        }
        Set<Student> students = studentsPerCourse.get(course);
        students.add(student);
    }

    public void assignStudentsToCourse(Set<Student> students, String course) {
        if (!studentsPerCourse.containsKey(course)) {
            throw new IllegalArgumentException("Cannot assign student to course which do not exists.");
        }
        Set<Student> sts = studentsPerCourse.get(course);
        for (Student student : students) {
            sts.add(student);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(getEmail(), teacher.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getEmail());
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", birth=" + birth +
                ", studentsPerCourse=" + studentsPerCourse +
                '}';
    }
}
