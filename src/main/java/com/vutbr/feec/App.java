package com.vutbr.feec;

import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.Arrays;

import com.vutbr.feec.db.Database;
import com.vutbr.feec.io.WriteToFile;
import com.vutbr.feec.model.Student;
import com.vutbr.feec.model.Teacher;

/**
 * @author Pavel Seda
 */
public class App {

    private static final String MAVEN_RESOURCES_PREFIX = "./src/main/resources/";

    public static void main(String[] args) {
        Student pepa = new Student(1L, "pepa@email.cz", "Pepek", LocalDate.of(1985, Month.APRIL, 25));
        Student karel = new Student(2L, "karel@email.cz", "Karlos", Year.of(2001).atMonth(Month.AUGUST).atDay(20));
        Student milos = new Student(3L, "milos@email.cz", "Milous", LocalDate.of(1999, 3, 21));
        Student martin = new Student(4L, "martin@email.cz", "Martin", LocalDate.now());
        Student pavel = new Student(5L, "pavel@email.cz", "Pavel", LocalDate.of(2002, 4, 10));

        Database db = new Database();
        db.setStudents(Arrays.asList(pepa, karel, milos, martin, pavel));
        db.printAllStudents();

        db.removeStudentById(1);

        System.out.println(
                System.lineSeparator() + "Print all students without student with ID 1.." + System.lineSeparator());

        db.printAllStudents();

        // uloží databáze studentů do souboru
        WriteToFile writeStudentsToFile = new WriteToFile();
        writeStudentsToFile.writeToFile(new File(MAVEN_RESOURCES_PREFIX + "students.csv"), db.getStudents());

        Teacher pavelTeacher = new Teacher(1L, "pepa@email.cz", "Namornik", Year.of(1988).atMonth(Month.AUGUST).atDay(20));
        pavelTeacher.addCourseAndStudent("bpc2t", karel);
        pavelTeacher.addCourseAndStudent("bkom", martin);
    }
}
